import os

dataset_origem = 'datasets/csv/'
dataset_destino = 'datasets/tratados/'

for csv_file in sorted(os.listdir(dataset_origem)):
    prefix_name = csv_file.split('.')[0]
    with open(dataset_origem + csv_file,'r', encoding='ISO-8859-1') as input_file:
        next(input_file)
        for line in input_file:
            converted_line = line.strip().replace('  ',';').replace('\n','')
            campos = []
            campos.append(converted_line.split(';')[0].strip()) # regiao
            campos.append(converted_line.split(';')[1].strip()) # estado
            campos.append(converted_line.split(';')[2].strip()) # municipio
            campos.append(converted_line.split(';')[-7].strip()) # revenda
            campos.append(converted_line.split(';')[-6].strip()) # produto
            ano = (converted_line.split(';')[-5].split('/')[2])
            mes = str(converted_line.split(';')[-5].split('/')[1])
            campos.append(ano) # ano
            campos.append(mes) # mes
            data = ano + '-' + mes
            campos.append(data) #periodo
            campos.append(converted_line.split(';')[-3].strip()) # valor_venda
            campos.append(converted_line.split(';')[-2].strip()) # unidade
            campos.append(converted_line.split(';')[-1].strip()) # bandeira
            new_line = ";".join(campos)
            with open(dataset_destino + ano + '-output.csv','a') as output_file:
                print(new_line, file = output_file)
