import requests

# dados abertos do preço dos combustiveis automativos
# endpoint segue o padrao abaixo
# http://www.anp.gov.br/images/dadosabertos/precos/2008-1_CA.csv

url_base = "http://www.anp.gov.br/images/dadosabertos/precos/"
dir_dataset = 'datasets/csv/'

for ano in range(2009,2019):
    for semestre in range (1,3):
        arquivo_local = str(ano) + "-" + str(semestre) + "_CA.csv"
        endpoint = url_base + str(ano) + "-" + str(semestre) + "_CA.csv"
        r = requests.get(endpoint)
        with open(dir_dataset + arquivo_local,'wb') as arquivo:
            arquivo.write(r.content)
