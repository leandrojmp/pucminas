import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
sns.set()
#
# diretorios
dataset_dir = "datasets/tratados/"
dataset_file = "consolidado.csv"
img_dir = "imagens/"
#
# importa dataset
dados = pd.read_csv(dataset_dir + dataset_file, sep=';', header=None, decimal=',')
dados.columns = ['regiao','estado','municipio','revenda',
                 'produto','ano','mes','periodo','valor_venda',
                 'unidade','bandeira']
#
# listas
regioes = ['CO','N','NE','S','SE']
centrooeste  = ['DF','GO','MS','MT']
norte = ['AC', 'AM', 'AP', 'PA', 'RO', 'RR', 'TO']
nordeste = ['AL', 'BA', 'CE', 'MA', 'PB', 'PE', 'PI', 'RN', 'SE']
sul = ['PR', 'RS', 'SC']
sudeste = ['ES', 'MG', 'RJ', 'SP']
estados = ['DF', 'GO', 'MS', 'MT', 'AC', 'AM', 'AP', 'PA', 'RO', 'RR', 'TO', 'AL', 'BA', 'CE', 'MA', 'PB', 'PE', 'PI', 'RN', 'SE', 'PR', 'RS', 'SC', 'ES', 'MG', 'RJ', 'SP']
anos = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]
meses = [1,2,3,4,5,6,7,8,9,10,11,12]
#
# funções
#
# grafico consolidado por ano
def anual():
    arq_imagem = img_dir + "por-ano.png"
    titulo_grafico = "Preço por Ano"
    anos = [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018]
    media_por_ano = list(dados.groupby('ano')['valor_venda'].mean())
    max_por_ano = list(dados.groupby('ano')['valor_venda'].max())
    min_por_ano = list(dados.groupby('ano')['valor_venda'].min())
    dados_por_ano = { 'ano': pd.Series(anos), 'media': pd.Series(media_por_ano), 
                      'max': pd.Series(max_por_ano) , 'min': pd.Series(min_por_ano) }
    df_dados_por_ano = pd.DataFrame(dados_por_ano)
    valores = list(np.arange(0,7, 0.2))
    graf_por_ano = df_dados_por_ano.plot(kind='line', x ='ano', xticks=anos, yticks=valores, fontsize=6)
    graf_por_ano.set_title(titulo_grafico)
    graf_por_ano.set_xlabel("Mês")
    graf_por_ano.set_ylabel("Preço")
    plt.tight_layout()
    plt.savefig(arq_imagem, format="png", dpi=150)
    plt.close('all')
#
# grafico consolidado por mês para cada ano
def mensal(produto = "todos"):
    for ano in anos:
        if produto == "todos":
            dados_ano = dados.loc[dados['ano'].isin([ano])]
            titulo_grafico = "Preço por Mês (" + str(ano) + ")"
        else:
            dados_ano = dados.loc[dados['ano'].isin([ano])]
            dados_ano = dados_ano.loc[dados_ano['produto'] == produto]
            titulo_grafico = "Preço por Mês " + produto + " (" + str(ano) + ")"
        arq_imagem = img_dir + "por-mes-" + produto + "-" + str(ano) + ".png"
        meses = [1,2,3,4,5,6,7,8,9,10,11,12]
        media_por_mes = list(dados_ano.groupby('mes')['valor_venda'].mean())
        max_por_mes = list(dados_ano.groupby('mes')['valor_venda'].max())
        min_por_mes = list(dados_ano.groupby('mes')['valor_venda'].min())
        dados_por_mes = { 'mes': pd.Series(meses), 'média': pd.Series(media_por_mes), 'máximo': pd.Series(max_por_mes) , 'mínimo': pd.Series(min_por_mes) }
        df_dados_por_mes = pd.DataFrame(dados_por_mes)
        valores = list(np.arange(0,7, 0.2))
        graf_por_mes = df_dados_por_mes.plot(kind='line', x ='mes', xticks=meses, yticks=valores, fontsize=6)
        graf_por_mes.set_title(titulo_grafico)
        graf_por_mes.set_xlabel("Mês")
        graf_por_mes.set_ylabel("Preço")
        plt.tight_layout()
        plt.savefig(arq_imagem, format="png", dpi=150)
        plt.close('all')
#
#
def mensal_por_ano(produto = "todos"):
    meses = [1,2,3,4,5,6,7,8,9,10,11,12]
    titulo_grafico = "Preço Médio por Mês " + produto
    arq_imagem = img_dir + "medio-por-mes-" + produto + ".png"
    dados_por_mes = {}
    dados_por_mes['mes'] = pd.Series(meses)
    for ano in anos:
        if produto == "todos":
            dados_ano = dados.loc[dados['ano'].isin([ano])]
            media_por_mes = list(dados_ano.groupby('mes')['valor_venda'].mean())
            dados_por_mes[str(ano)] = pd.Series(media_por_mes)
        else:
            dados_ano = dados.loc[dados['ano'].isin([ano])]
            dados_ano = dados_ano.loc[dados_ano['produto'] == produto]
            media_por_mes = list(dados_ano.groupby('mes')['valor_venda'].mean())
            dados_por_mes[str(ano)] = pd.Series(media_por_mes)
    df_dados_por_mes = pd.DataFrame(dados_por_mes)
    valores = list(np.arange(0,7, 0.2))
    graf_por_mes = df_dados_por_mes.plot(kind='line', x ='mes', xticks=meses, yticks=valores, fontsize=6)
    graf_por_mes.set_title(titulo_grafico)
    graf_por_mes.set_xlabel("Mês")
    graf_por_mes.set_ylabel("Preço")
    plt.tight_layout()
    plt.legend(loc='upper right', ncol=3)
    plt.savefig(arq_imagem, format="png", dpi=150)
    plt.close('all')
#
# anual por estado e regiao
def anual_por_estado(regiao,produto = "todos"):
    titulo_grafico = "Preço Médio por Ano " + produto
    arq_imagem = img_dir + "media-por-ano-" + "-".join(regiao) + "-" + produto + ".png"
    dados_por_ano = {}
    dados_por_ano['ano'] = pd.Series(anos)
    for uf in regiao:
        if produto == "todos":
            dados_ano = dados.loc[dados['estado'].isin([uf])]
            media_por_ano = list(dados_ano.groupby('ano')['valor_venda'].mean())
            dados_por_ano[uf] = pd.Series(media_por_ano)
        else:
            dados_ano = dados.loc[dados['estado'].isin([uf])]
            dados_ano = dados_ano.loc[dados_ano['produto'] == produto]
            media_por_ano = list(dados_ano.groupby('ano')['valor_venda'].mean())
            dados_por_ano[uf] = pd.Series(media_por_ano)
    df_dados_por_ano = pd.DataFrame(dados_por_ano)
    valores = list(np.arange(0,7, 0.2))
    graf_por_ano = df_dados_por_ano.plot(kind='line', x ='ano', xticks=anos, yticks=valores, fontsize=6)
    graf_por_ano.set_title(titulo_grafico)
    graf_por_ano.set_xlabel("Ano")
    graf_por_ano.set_ylabel("Preço")
    plt.tight_layout()
    plt.legend(loc='upper right', ncol=3)
    plt.savefig(arq_imagem, format="png", dpi=150)
    plt.close('all')
# grafico por regiao
def regiao_mensal(tipo,produto = "todos"):
    for ano in anos:
        if produto == "todos":
            dados_ano = dados.loc[dados['ano'].isin([ano])]
        else:
            dados_ano = dados.loc[dados['ano'].isin([ano])]
            dados_ano = dados_ano.loc[dados_ano['produto'] == produto]
        meses = [1,2,3,4,5,6,7,8,9,10,11,12]
        dados_N = dados_ano.loc[dados_ano['regiao'].isin(['N'])]
        dados_NE = dados_ano.loc[dados_ano['regiao'].isin(['NE'])]
        dados_S = dados_ano.loc[dados_ano['regiao'].isin(['S'])]
        dados_SE = dados_ano.loc[dados_ano['regiao'].isin(['SE'])]
        dados_CO = dados_ano.loc[dados_ano['regiao'].isin(['CO'])]
        meses = [1,2,3,4,5,6,7,8,9,10,11,12]
        if tipo == "media":
            arq_imagem = img_dir + "media-regiao-mensal-" + produto + "-" + str(ano) + ".png"
            titulo_grafico = "Preço Médio por Mês " + produto + " (" + str(ano) + ")"
            mes_n = list(dados_N.groupby('mes')['valor_venda'].mean())
            mes_ne = list(dados_NE.groupby('mes')['valor_venda'].mean())
            mes_s = list(dados_S.groupby('mes')['valor_venda'].mean())
            mes_se = list(dados_SE.groupby('mes')['valor_venda'].mean())
            mes_co = list(dados_CO.groupby('mes')['valor_venda'].mean())
        if tipo == "maximo":
            arq_imagem = img_dir + "maximo-regiao-mensal-" + produto + "-" + str(ano) + ".png"
            titulo_grafico = "Preço Máximo por Mês " + produto + " (" + str(ano) + ")"
            mes_n = list(dados_N.groupby('mes')['valor_venda'].max())
            mes_ne = list(dados_NE.groupby('mes')['valor_venda'].max())
            mes_s = list(dados_S.groupby('mes')['valor_venda'].max())
            mes_se = list(dados_SE.groupby('mes')['valor_venda'].max())
            mes_co = list(dados_CO.groupby('mes')['valor_venda'].max())
        if tipo == "minimo":
            arq_imagem = img_dir + "minimo-regiao-mensal-" + produto + "-" + str(ano) + ".png"
            titulo_grafico = "Preço Mínimo por Mês " + produto + " (" + str(ano) + ")"
            mes_n = list(dados_N.groupby('mes')['valor_venda'].min())
            mes_ne = list(dados_NE.groupby('mes')['valor_venda'].min())
            mes_s = list(dados_S.groupby('mes')['valor_venda'].min())
            mes_se = list(dados_SE.groupby('mes')['valor_venda'].min())
            mes_co = list(dados_CO.groupby('mes')['valor_venda'].min())
        dados_por_mes = { 'mes': pd.Series(meses), 
                          'norte': pd.Series(mes_n),
                          'nordeste': pd.Series(mes_ne),
                          'sul': pd.Series(mes_s),
                          'sudeste': pd.Series(mes_se), 
                          'centro oeste': pd.Series(mes_co) }
        df_dados_por_mes = pd.DataFrame(dados_por_mes)
        valores = list(np.arange(1.6,7, 0.2))
        graf_por_mes = df_dados_por_mes.plot(kind='line', x ='mes', xticks=meses, yticks=valores)
        graf_por_mes.set_title(titulo_grafico)
        graf_por_mes.set_xlabel("Mês")
        graf_por_mes.set_ylabel("Preço")
        plt.tight_layout()
        plt.savefig(arq_imagem, format="png", dpi=150)
        plt.close('all') 
# por produto em 2018

def produtos(ano):
    arq_imagem = img_dir + "por-produtos-" + str(ano) + ".png"
    titulo_grafico = "Preço por Mês (" + str(ano) + ")"
    dados_ano = dados.loc[dados['ano'].isin([ano])]
    meses = [1,2,3,4,5,6,7,8,9,10,11,12]
    dados_gasolina = dados_ano.loc[dados_ano['produto'].isin(['GASOLINA'])]
    dados_etanol = dados_ano.loc[dados_ano['produto'].isin(['ETANOL'])]
    dados_diesel = dados_ano.loc[dados_ano['produto'].isin(['DIESEL','DIESEL S10','DIESEL S50'])]
    dados_gnv = dados_ano.loc[dados_ano['produto'].isin(['GNV'])]
    max_mes_gasolina = list(dados_gasolina.groupby('mes')['valor_venda'].max())
    max_mes_etanol = list(dados_etanol.groupby('mes')['valor_venda'].max())
    max_mes_diesel = list(dados_diesel.groupby('mes')['valor_venda'].max())
    max_mes_gnv = list(dados_gnv.groupby('mes')['valor_venda'].max())
    dados_por_mes = { 'mes': pd.Series(meses), 
                      'max gasolina': pd.Series(max_mes_gasolina), 
                      'max etanol': pd.Series(max_mes_etanol) , 
                      'max diesel': pd.Series(max_mes_diesel) , 
                      'max gnv': pd.Series(max_mes_gnv) }
    df_dados_por_mes = pd.DataFrame(dados_por_mes)
    valores = list(np.arange(3,7, 0.2))
    graf_por_mes = df_dados_por_mes.plot(kind='line', x ='mes', xticks=meses, yticks=valores)
    graf_por_mes.set_title(titulo_grafico)
    graf_por_mes.set_xlabel("Mês")
    graf_por_mes.set_ylabel("Preço")
    plt.tight_layout()
    plt.savefig(arq_imagem, format="png", dpi=150)
    plt.close('all') 
#
# identifica localidade com preço maximo
def maximo(ano,mes):
    dados2018 = dados.loc[dados['ano'].isin([ano])]
    dadosFinal = dados2018.loc[dados2018['mes'].isin([mes])]
    dadosFinalG = dadosFinal.loc[dadosFinal['produto'].isin(["GASOLINA"])]
    dadosFinalD = dadosFinal.loc[dadosFinal['produto'].isin(["DIESEL"])]
    print(dadosFinalG[dadosFinalG['valor_venda'] == dadosFinalG['valor_venda'].max()])
    print(dadosFinalD[dadosFinalD['valor_venda'] == dadosFinalD['valor_venda'].max()])

def minimo(ano,mes):
    dados2018 = dados.loc[dados['ano'].isin([ano])]
    dadosFinal = dados2018.loc[dados2018['mes'].isin([mes])]
    dadosFinalG = dadosFinal.loc[dadosFinal['produto'].isin(["GASOLINA"])]
    dadosFinalD = dadosFinal.loc[dadosFinal['produto'].isin(["DIESEL"])]
    print(dadosFinalG[dadosFinalG['valor_venda'] == dadosFinalG['valor_venda'].min()])
    print(dadosFinalD[dadosFinalD['valor_venda'] == dadosFinalD['valor_venda'].min()])
